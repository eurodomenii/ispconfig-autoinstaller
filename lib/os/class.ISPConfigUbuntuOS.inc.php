<?php
/**
 * Description of class
 *
 * @author croydon
 */
class ISPConfigUbuntuOS extends ISPConfigDebianOS {
	protected function configureApt() {
		// enable contrib and non-free
		ISPConfigLog::info('Configuring apt repositories.', true);

		$contents = '# created by ISPConfig auto installer
deb http://de.archive.ubuntu.com/ubuntu/ bionic main restricted
deb http://de.archive.ubuntu.com/ubuntu/ bionic-updates main restricted
deb http://de.archive.ubuntu.com/ubuntu/ bionic universe
deb http://de.archive.ubuntu.com/ubuntu/ bionic-updates universe
deb http://de.archive.ubuntu.com/ubuntu/ bionic multiverse
deb http://de.archive.ubuntu.com/ubuntu/ bionic-updates multiverse
deb http://de.archive.ubuntu.com/ubuntu/ bionic-backports main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu bionic-security main restricted
deb http://security.ubuntu.com/ubuntu bionic-security universe
deb http://security.ubuntu.com/ubuntu bionic-security multiverse
		';
		file_put_contents('/etc/apt/sources.list', $contents);
	}

	protected function beforePackageInstall($section = '') {
		$this->stopService('apparmor');
		$this->stopService('sendmail');

		$cmd = 'update-rc.d -f apparmor remove ; update-rc.d -f sendmail remove ; apt-get -y -qq remove apparmor apparmor-utils';
		$result = $this->exec($cmd);
		if($result === false) {
			throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
		}
	}

	protected function afterPackageInstall($section = '') {
		if($section === 'mail') {
			$cmd = 'freshclam';
			$result = $this->exec($cmd, array(62));
			if($result === false) {
				//throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
			}

			$this->startService('clamav-daemon');
		}
	}

	protected function addSuryRepo() {
		ISPConfigLog::info('Activating sury php repository.', true);

		$cmd = 'add-apt-repository -y ppa:ondrej/php';
		$result = $this->exec($cmd);
		if($result === false) {
			throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
		}
	}

	protected function getFail2BanJail() {
		$jk_jail = '[pure-ftpd]
enabled = true
port = ftp
filter = pure-ftpd
logpath = /var/log/syslog
maxretry = 3

[dovecot]
enabled = true
filter = dovecot
action = iptables-multiport[name=dovecot-pop3imap, port="pop3,pop3s,imap,imaps", protocol=tcp]
logpath = /var/log/mail.log
maxretry = 5

[postfix-sasl]
enabled = true
port = smtp
filter = postfix
logpath = /var/log/mail.log
maxretry = 3';
		return $jk_jail;
	}

	protected function setDefaultPHP() {
		ISPConfigLog::info('Setting default system php version.', true);
		$cmd = 'update-alternatives --set php /usr/bin/php7.2';
		$result = $this->exec($cmd);
		if($result === false) {
			throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
		}

		if(ISPConfig::shallInstall('web')) {
			// When --use-php-system is used, there is no alternative for php-fpm.sock.
			if(ISPConfig::wantsPHP() === 'system') {
				$cmd = 'update-alternatives --set php-cgi /usr/bin/php-cgi7.2';
			} else {
				$cmd = 'update-alternatives --set php-cgi /usr/bin/php-cgi7.2 ; update-alternatives --set php-fpm.sock /run/php/php7.2-fpm.sock';
			}
			$result = $this->exec($cmd);
			if($result === false) {
				throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
			}
		}
	}

	protected function getSystemPHPVersion() {
		return '7.2';
	}

}
